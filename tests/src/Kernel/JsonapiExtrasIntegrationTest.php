<?php

namespace Drupal\Tests\jsonapi_cross_bundles\Kernel;

/**
 * Tests the integration with JSON:API Extras.
 *
 * @group jsonapi_cross_bundles
 * @requires module jsonapi_extras
 *
 * @see https://www.drupal.org/project/jsonapi_extras/issues/3068811
 */
final class JsonapiExtrasIntegrationTest extends JsonapiCrossBundlesTestBase {
  /**
   * {@inheritdoc}
   */
  public static $modules = ['jsonapi_extras'];

  /**
   * Tests our decoration does not crash with JSON:API Extras installed.
   */
  public function testResourceTypeRepositoryDefinition() {
    $resource_type_repository = $this->container->get('jsonapi.resource_type.repository');
    $this->assertTrue(TRUE, 'JSON:API Extras did not cause a conflicting crash.');
  }

}
