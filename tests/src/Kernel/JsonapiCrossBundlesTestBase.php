<?php

namespace Drupal\Tests\jsonapi_cross_bundles\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Base test class for JSON:API Cross Bundles.
 */
abstract class JsonapiCrossBundlesTestBase extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['serialization', 'jsonapi', 'jsonapi_cross_bundles'];

}
